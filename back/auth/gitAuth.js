'use strict';
const passport = require('passport');
const GitHubStrategy = require('passport-github2').Strategy;
const utils = require('../lib/utils');
const User = require('../models/userModel');

passport.use(new GitHubStrategy({
        clientID: 'fcec3105634afe7e976c',
        clientSecret: '51eeec3a84ecda100ff00c0be722dbf574c87837',
        callbackURL: "http://localhost:8000/user/auth/github/callback",
        scope:['user:email','repo','repo:status','repo_deployment', 'public_repo','repo:invite', 'write:repo_hook', 'read:repo_hook', 'admin:org', 'write:org','read:org','workflow']
    },
   function(accessToken, refreshToken, profile, done) {

        User.findOne({
            'login': profile._json.login
        }, function(err, user) {
            if (err) {
                return done(err);
            }

            if (!user) {

                const nameAR = (profile._json.name != null ? profile._json.name.split(" ") : '');
                const saltHash = utils.genPassword(profile.id);
                const salt = saltHash.salt;
                const hash = saltHash.hash;
                const git_id = profile.id;
                const login = profile._json.login.toString();
                const firstname = (nameAR[0] != 'undefined' ? nameAR[0] : '');
                const lastname = (nameAR[1] != 'undefined' ? nameAR[1] : '');
                const api_url= profile._json.url;
                const avatar_url = profile._json.avatar_url;
                const email_git = (profile.emails[0].value != 'undefined' ? profile.emails[0].value : '');
                const token_git = accessToken;


                user = new User({git_id:git_id, login: login, firstname: firstname, lastname: lastname, email_git:email_git,api_url:api_url,avatar_url: avatar_url,hash:hash, salt:salt, token_git: token_git});
                user.save(function (err) {
                return done(err, user);
                });


            } else {
                if(accessToken.length > 0){

                    User.findOneAndUpdate({'_id' : user._id},{token_git: accessToken},'',function(err, user){
                        return done(err, user);
                    })
                }
                return done(err, user);
            }
        });
    }
    ))
passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});
module.exports = passport;