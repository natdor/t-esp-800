'use strict';
module.exports = function(app) {

const utils = require('../lib/utils');
const User = require('../models/userModel');
const passportGit = require('../auth/gitAuth');
const passportJWT = require('../auth/jwtAuth');
const userController = require('../controllers/userController');
const projectsController = require('../controllers/projectsController');
const collaboratorsController = require('../controllers/collaboratorsController');
const designsController = require('../controllers/designController');
const commentsController = require('../controllers/commentsController');
const repoController = require('../controllers/repoController');
const pullRequestController = require('../controllers/pullRequestController');
const teamsController = require('../controllers/teamsController');
const kanbansController = require('../controllers/kanbansController');

/**
     *  Regular Login
     */

    app.route('/user/login').post(userController.users_login);

    /**
     *  Github Login
     */

    app.route('/user/auth/github').get( passportGit.authenticate('github'));
    app.route('/user/auth/github/callback').get( function(req, res, next) {
        passportGit.authenticate('github', function(err, user, info) {
            if (err) { return next(err); }
            User.findOne({git_id: user.git_id})
                .then((user)=> {
                    //console.log(user);
                    if(!user){
                        res.status(401).json({success: false, msg:"could not find user"});
                    }
					
					console.log(user.git_id);

                    const isvalid = utils.validPassword(user.git_id.toString(), user.hash, user.salt);

                    if(isvalid){
                        const tokenObject = utils.issueJWT(user);
                     //res.status(200).json({success: true,_id: user._id,token_git: user.token_git, token: tokenObject.token, expiresIn: tokenObject.expires});
                        let token = tokenObject.token.replace('Bearer ','');
                        //console.log(token);
                        res.redirect('http://localhost:3000/my-projects?token='+token+'&id_user='+user._id+'&login='+user.login);

                    }else{
                        res.status(401).json({success: false, msg: "you entrered the wrong password"});
                    }
                })
        })(req, res, next);
    });

    app.route('/user/logout').get(passportJWT.authenticate('jwt',{session:false}),function (req,res) {
        req.logout();
        res.json({success:true});
    });

    /**
     * Users routes
     */

    app.route('/users').get(passportJWT.authenticate('jwt',{session:false}), userController.users);
    app.route('/user/signup').post(userController.users_create);
    app.route('/user/:id_user/profile').get(passportJWT.authenticate('jwt',{session:false}), userController.users_profile);
    app.route('/user/edit').put(passportJWT.authenticate('jwt',{session:false}), userController.users_update);
    app.route('/user/:id_user/delete').delete(passportJWT.authenticate('jwt',{session:false}), userController.users_delete);


    /**
     * Collaborators route
     */

    app.route('/list/:id_user/collaborators/:repo_name').get(passportJWT.authenticate('jwt',{session:false}), collaboratorsController.collaborators);
    app.route('/add/collaborators').post(passportJWT.authenticate('jwt',{session:false}), collaboratorsController.collaborators_add);
    app.route('/delete/:id_user/collaborators/:repo_name/:username').delete(passportJWT.authenticate('jwt',{session:false}), collaboratorsController.collaborators_delete);

    /**
     * Repo route
     */

    app.route('/list/:id_user/repos').get(passportJWT.authenticate('jwt',{session:false}), repoController.repos);
    app.route('/:id_user/repo/:repo_name').get(passportJWT.authenticate('jwt',{session:false}), repoController.repo);
    app.route('/add/repos').post(passportJWT.authenticate('jwt',{session:false}), repoController.repos_create);
    app.route('/edit/repos').patch(passportJWT.authenticate('jwt',{session:false}), repoController.repos_update);
    app.route('/delete/:id_user/repos/:repo_name').delete(passportJWT.authenticate('jwt',{session:false}), repoController.repos_delete);

    /**
     * Repo file
     */


    app.route('/:id_user/repo/files/:repo_name').get(passportJWT.authenticate('jwt',{session:false}), repoController.repos_files);
    app.route('/:id_user/repo/file/:repo_name/:path').get(passportJWT.authenticate('jwt',{session:false}), repoController.repos_file);
    app.route('/repo/file').put(passportJWT.authenticate('jwt',{session:false}), repoController.repos_file_update_or_create);


    /**
     * Projects route
     */ 

    app.route('/list/:id_user/:repo_name/projects').get(passportJWT.authenticate('jwt',{session:false}), projectsController.projects);
    app.route('/:id_user/project/:project_id').get(passportJWT.authenticate('jwt',{session:false}), projectsController.project);
    app.route('/add/projects').post(passportJWT.authenticate('jwt',{session:false}), projectsController.projects_create);
    app.route('/edit/projects').put(passportJWT.authenticate('jwt',{session:false}), projectsController.projects_update);
    app.route('/delete/:id_user/projects/:project_id').delete(passportJWT.authenticate('jwt',{session:false}), projectsController.projects_delete);
    app.route('/:id_user/projects/:project_id/projects').get(passportJWT.authenticate('jwt',{session:false}), projectsController.columns);
    app.route('/:id_user/projects/columns/:column_id/cards').get(passportJWT.authenticate('jwt',{session:false}), projectsController.cards);


    /**
     * pullRequest
     */

    app.route('/list/:id_user/pulls/:repo').get(passportJWT.authenticate('jwt',{session:false}), pullRequestController.pullRequests);
    app.route('/add/pulls').post(passportJWT.authenticate('jwt',{session:false}), pullRequestController.pullRequests_create);
    app.route('/edit/pulls').put(passportJWT.authenticate('jwt',{session:false}), pullRequestController.pullRequests_update);
    app.route('/merge/pulls').post(passportJWT.authenticate('jwt',{session:false}), pullRequestController.pullRequests_merge);

    /**
     * Designs
     */

    app.route('/list/designs').get(passportJWT.authenticate('jwt',{session:false}), designsController.designs);
    app.route('/:id_design/design').get(passportJWT.authenticate('jwt',{session:false}), designsController.design);
    app.route('/add/designs').post(passportJWT.authenticate('jwt',{session:false}), designsController.designs_create);
    app.route('/edit/designs').patch(passportJWT.authenticate('jwt',{session:false}), designsController.designs_update);
    app.route('/delete/:id_design/designs').delete(passportJWT.authenticate('jwt',{session:false}), designsController.designs_delete);

    /**
     * Comments
     */

    app.route('/list/comments/:design_id').get(passportJWT.authenticate('jwt',{session:false}), commentsController.comments);
    app.route('/add/comments').post(passportJWT.authenticate('jwt',{session:false}), commentsController.comments_create);
    app.route('/edit/comments').patch(passportJWT.authenticate('jwt',{session:false}), commentsController.comments_update);
    app.route('/delete/:id_comment/comments').delete(passportJWT.authenticate('jwt',{session:false}), commentsController.comments_delete);

    /**
     * Teams
     */
    app.route('/teams').get(passportJWT.authenticate('jwt',{session:false}), teamsController.teams);
    app.route('/add/teams').post(passportJWT.authenticate('jwt',{session:false}), teamsController.teams_create);
    app.route('/edit/teams').patch(passportJWT.authenticate('jwt',{session:false}), teamsController.teams_update);
    app.route('/teams/:id/delete').delete(passportJWT.authenticate('jwt',{session:false}), teamsController.teams_delete);

    /**
     * Kanbans
     */

    app.route('/kanbans').get(passportJWT.authenticate('jwt',{session:false}), kanbansController.kanbans);
    app.route('/kanban/:kanbanId').get(passportJWT.authenticate('jwt',{session:false}), kanbansController.find_kanban);
    app.route('/kanban/create/:userId').post(passportJWT.authenticate('jwt',{session:false}), kanbansController.create_kanban);
    app.route('/kanban/update/:kanbanId').patch(passportJWT.authenticate('jwt',{session:false}), kanbansController.update_kanban);
    app.route('/kanban/delete/:kanbanId').delete(passportJWT.authenticate('jwt',{session:false}), kanbansController.delete_kanban);

};


