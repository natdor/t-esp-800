'use strict';
let mongoose = require('mongoose');
let commentsSchema = mongoose.Schema({
    content: String,
    author: String,
    id_user: String,
    design_id:String,
    create_date: {
        type: Date,
        default: Date.now
    }
},{collection : 'Comments'});

module.exports = mongoose.model('Comments', commentsSchema);