'use strict';
let mongoose = require('mongoose');
let teamsSchema = mongoose.Schema({
    name: String,
    members:Object,
    user_id: String,
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at:Date
},{collection : 'Teams '});

module.exports = mongoose.model('Teams', teamsSchema);