'use strict';
let mongoose = require('mongoose');
let designsSchema = mongoose.Schema({
    name: String,
    design_url: String,
    user_id: String,
    create_date: {
        type: Date,
        default: Date.now
    }
},{collection : 'Designs'});

module.exports = mongoose.model('Designs', designsSchema);