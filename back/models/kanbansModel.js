'use strict';
let mongoose = require('mongoose');

let kanbanSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    user_id: {
        type: String,
        required: true
    },
    lanes: {
        type: Object,
        required: false
    }
});

module.exports = mongoose.model('Kanban', kanbanSchema);