'use strict';
let mongoose = require('mongoose');
let chatsSchema = mongoose.Schema({
    message: String,
    name: String,
    id_team:String,
    create_date: {
        type: Date,
        default: Date.now
    }
},{collection : 'Chats'});

module.exports = mongoose.model('Chats', chatsSchema);