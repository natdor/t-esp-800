'use strict';
let mongoose = require('mongoose');
let projectSchema = mongoose.Schema({
    id: Number,
    name: String,
    body: String,
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: Date
},{collection : 'Project'});

module.exports = mongoose.model('Project', projectSchema);