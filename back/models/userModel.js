'use strict';
let mongoose = require('mongoose');
let userSchema = mongoose.Schema({
    git_id: Number,
    login:String,
    firstname:String,
    lastname:String,
    email:String,
	role:{type:String, default: "Developer"},
    avatar_url:String,
    api_url: String,
    hash: String,
    salt: String,
    token_git:String,
    create_date: {
        type: Date,
        default: Date.now
    }
},{collection : 'User'});

module.exports = mongoose.model('User', userSchema);