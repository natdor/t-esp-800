const express = require('express'),
 	app = express(),
	bodyParser = require('body-parser');
 PORT = process.env.PORT || 8000;
const socketio =  require('socket.io');
const http = require('http');

let passport = require('passport');
let session = require('express-session');
let cors = require('cors');
let flash = require('connect-flash');
let methodOverride = require('method-override');
let config = require('./db/config');
let db = require('./db/db');
swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger.json');
const {addUser, removeUser, getUser, getUsersInRoom, messages,getMessages} = require('./controllers/chatController');
db(config);



app.use(session({
	secret: 'secret',
	resave:true,
	saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(cors({

	origin: "http://localhost:3000",
}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(methodOverride());

//app.listen(port, () => console.log(`The api listening on port ${port}!`))

const server = http.createServer(app);
const io = socketio(server);
let routes = require('./routes/apiRoutes');
routes(app);


io.on('connect', (socket) => {
	socket.on('join', ({ name, room }, callback) => {
		const { error, user } = addUser({ id: socket.id, name, room });

		if(error) return callback(error);

		socket.join(user.room);

		socket.emit('message', { user: 'admin', text: `${user.name}, welcome to room ${user.room}.`});
		getMessages(socket,name);
		socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `${user.name} has joined!` });

		io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });

		callback();
	});

	socket.on("callUser", (data) => {
		io.to(data.userToCall).emit('hey', {signal: data.signalData, from: data.from});
	})

	socket.on("acceptCall", (data) => {
		io.to(data.to).emit('callAccepted', data.signal);
	})

	socket.on('sendMessage', (message,team_id, callback) => {
		const user = getUser(socket.id);

		messages(user.name, message,team_id);
		io.to(user.room).emit('message', { user: user.name, text: message, id_team: team_id });

		callback();
	});

	socket.on('disconnect', () => {
		const user = removeUser(socket.id);

		if(user) {
			io.to(user.room).emit('message', { user: 'Admin', text: `${user.name} has left.` });
			io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room)});
		}
	})
});


server.listen(PORT, () => console.log(`Serve has been started on port ${PORT}`));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1', routes);


