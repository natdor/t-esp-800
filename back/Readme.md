# T-ESP-800

# Run the api server

npx nodemon rs server.js

#accéder au container
docker exec -it api-db

# Connect to mongodb server
mongo admin -u root -p root

# Create the api database

use api-db
db.init.insert({"name":"creation"})

# Create new db user

db.createUser(  
  {
    user: "devapi",
    pwd: "devapi",
    roles: [ { role: "readWrite", db: "api-db" } ]
  }
)

# Connect to mongo for the api database

mongo -u devapi -p devapi --authenticationDatabase "api-db"

# Routes


## Regular Login route
     
### POST | /user/login
	
	{
        
        email:String,
		password:String,
		
	}

    
 ## Github Login route
     

### GET | /user/auth/github

	
 ## Users routes
     

### GET | /users
### GET | /user/logout

Before signup you have to create a personal acces token: https://github.com/settings/tokens

### POST | /user/signup
	
	{
        login:String,
        email:String,
        firstname:String,
        lastname:String,
		password:String,
		token_git:String
	}
	
### GET | /user/:id_user/profile
### PUT | /user/edit
	{
        login:String,
        email:String,
        firstname:String,
        lastname:String,
		password:String,
		token_git:String
	}
	
### DELETE | /user/:id_user/delete
    
 ## Collaborators routes
     
### GET | /list/:id_user/collaborators/:repo_name
### POST | /add/collaborators
	{
		id_user: String,
		repo_name: String,
		username: String
	}
### DELETE | /delete/:id_user/collaborators/:repo_name/:username

    
## Repo routes
     

### GET | /list/:id_user/repos
	
### GET | /:id_user/repo/:repo_name
	
### POST | /add/repos
	{
		id_user:String,
		name: String,
		description: String,
		homepage: String,
		private: Bool,
		has_issues: Bool,
		has_projects: Bool,
		has_wiki: Bool
	}
	
### PATCH | /edit/repos
	
	{
		id_user:String,
		repo_name: String,
		name: String,
		description: String,
		homepage: String,
		private: Bool,
		has_issues: Bool,
		has_projects: Bool,
		has_wiki: Bool
	}
	
### DELETE | /delete/:id_user/repos/:repo_name
	
    
     ## Repo file
     


### GET | /:id_user/repo/files/:repo_name
### GET | /:id_user/repo/file/:repo_name/:path
### PUT | /repo/file
	{
	    path: String,
		repo_name: String,
		message: String,
		content: String,
		sha: String,
		branch: String,
		committer: String,
		author: String
	}
    
    ## Projects routes
     

### GET | /list/:id_user/:repo_name/projects
### GET | /:id_user/project/:project_id
### POST | /add/projects
	{
		id_user:String,
		repo_name: String,
		name: req.body.name,
		body: req.body.description

	}
### PUT | /edit/projects
	{
		id_user:String,
		repo_name: String,
		name: req.body.name,
		body: req.body.description

	}
### DELETE | /delete/:id_user/projects/:project_id


    
## pullRequest routes
     

### GET | /list/:id_user/pulls/:repo
### POST | /add/pulls
	{
		id_user:String,
		repo_name: String,
		title: String,
		body: String,
		state: String,
		base: String
	}
	
### PUT | /edit/pulls
	{
		id_user:String,
		repo_name: String,
		pull_num:String,
		title: String,
		body: String,
		state: String,
		base: String
	}
	
### POST | /merge/pulls
	
	{
		id_user:String,
		repo_name: String,
		pull_num:String,
		commit_title: String,
		commit_message: String,
		sha: String,
		merge_method: String
	}

    
 ## Designs routes
     
	 
### GET |/list/designs
### GET | /:id_design/design
### POST | /add/designs
	
	{		
	name: String,
	design_url: String,
	user_id: String
	
	}
	
### PATCH | /edit/designs
	
	{		
	design_id: String,
	name: String,
	design_url: String,
	user_id: String
	}
### DELETE | /delete/:id_design/designs

    
## Comments routes
     

### GET |/list/comments/:design_id
### POST | /add/comments
	{
		content: String,
		design_id:String,
		id_user: String
	}
### PATCH | /edit/comments
	{
	content: String,
    comment_id: String
	}
### DELETE | /delete/:id_comment/comments
