const User = require('../models/userModel');
const axios = require('axios');

exports.issues = function (req,res) {
    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/issues',{
                    headers: {
                        'accept': 'application/vnd.github.machine-man-preview',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        res.json(response.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}

exports.issue = function(req,res){
    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/repos/'+user.login+'/'+req.params.repo+'/issues/'+req.params.issue_number, {

                    headers: {
                        'accept': 'application/vnd.github.machine-man-preview',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        res.json(response.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}

exports.create_issue = function(req,res){
    User.findById(req.body.id_user)
        .then((user)=> {
            if (user) {
                axios.post('https://api.github.com/repos/'+user.login+'/'+req.body.repo_name+'/issues', {

                        title: req.body.name,
                        body: req.body.description,
                        assignees: req.body.assignees,
                        milestone: req.body.milestone,
                        labels: req.body.labels

                    }, {
                        headers: {
                            'accept': 'application/vnd.github.machine-man-preview',
                            Authorization: `Bearer ${user.token_git}`}
                    }
                )
                    .then(function (response) {

                        res.json({success: true, message: "a new issue has been create"});


                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}

exports.update_issue = function(req,res){
    User.findById(req.body.id_user)
        .then((user)=> {
            if (user) {
                axios.patch('https://api.github.com/repos/'+user.login+'/'+req.body.repo_name+'/issues/'+req.body.issue_number, {

                        title: req.body.name,
                        body: req.body.description,
                        assignees: req.body.assignees,
                        milestone: req.body.milestone,
                        state: req.body.state,
                        labels: req.body.labels

                    }, {
                        headers: {
                            'accept': 'application/vnd.github.machine-man-preview',
                            Authorization: `Bearer ${user.token_git}`}
                    }
                )
                    .then(function (response) {

                        res.json({success: true, message: "a new issue has been create"});


                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}

exports.lock_issue = function(req,res){
    User.findById(req.body.id_user)
        .then((user)=> {
            if (user) {
                axios.put('https://api.github.com/repos/'+user.login+'/'+req.body.repo_name+'/issues/'+req.body.issue_number+'/lock', {

                        locked: req.body.locked,
                        active_lock_reason: req.body.active_lock_reason
                    }, {
                        headers: {
                            'accept': 'application/vnd.github.sailor-v-preview+json',
                            Authorization: `Bearer ${user.token_git}`}
                    }
                )
                    .then(function (response) {

                        res.json({success: true, message: "a new issue has been create"});


                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}

exports.unlock_issue = function(req,res){
    User.findById(req.body.id_user)
        .then((user)=> {
            if (user) {
                axios.delete('https://api.github.com/repos/'+user.login+'/'+req.params.repo_name+'/issues/'+req.params.issue_number+'/lock', {
                        headers: {
                            'accept': 'application/vnd.github.sailor-v-preview+json',
                            Authorization: `Bearer ${user.token_git}`}
                    }
                )
                    .then(function (response) {

                        res.json({success: true, message: "a new issue has been create"});


                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}