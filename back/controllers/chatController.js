const  Chat = require('../models/chatsModel');
const users = [];

const addUser = ({ id, name, room }) => {
    name = name.trim().toLowerCase();
    room = room.trim().toLowerCase();

    const existingUser = users.find((user) => user.room === room && user.name === name);

    if(!name || !room) return { error: 'Username and room are required.' };
    if(existingUser) return { error: 'Username is taken.' };

    const user = { id, name, room };

    users.push(user);

    return { user };
}

const messages = (name,message, team_id)=>{
    const chat = new Chat({name:name,message:message,id_team:team_id});

    chat.save();

}

const getMessages = (socket,user)=>{
    Chat.find({}, function (err,messages) {

        if(err){
            throw err;
        }
        messages.forEach((message)=>{
            if(message.name == user){
                socket.emit('message', { user: user, text: message.message, id_team: message.id_team  });
            }else{
                socket.emit('message', { user: message.name, text: message.message, id_team: message.id_team  });
            }
        })

    })
}

const removeUser = (id) => {
    const index = users.findIndex((user) => user.id === id);

    if(index !== -1) return users.splice(index, 1)[0];
}

const getUser = (id) => users.find((user) => user.id === id);

const getUsersInRoom = (room) => users.filter((user) => user.room === room);

module.exports = { addUser, removeUser, getUser, getUsersInRoom,messages,getMessages };