const axios = require('axios');
const User = require('../models/userModel');

exports.pullRequests = function(req,res){
    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/users/repos/' + user.login + '/' + req.params.repo + '/pulls',{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        res.json({id:response.data.id, number:response.data.number, sha:response.data.head.sha, success: true});
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
            });
}

exports.pullRequests_create = function (req,res) {
    User.findById(req.body.id_user)
        .then((user)=> {
            if (user) {
                axios.post('https://api.github.com/repos/' + user.login + '/' + req.body.repo_name + '/pulls', {
                    title: req.body.title,
                    body: req.body.description,
                    head: req.body.head,
                    base: req.body.base
                },{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        res.json(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}


exports.pullRequests_update = function (req,res) {
    User.findById(req.body.id_user)
        .then((user) => {
            if (user) {
                axios.put('https://api.github.com/repos/' + user.login + '/' + req.body.repo_name + '/pulls/' + req.body.pull_num + '/update-branch', {
                    title: req.body.title,
                    body: req.body.description,
                    state: req.body.state,
                    base: req.body.base
                },{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        res.json({message: response.data.message, url: response.data.url});
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}
    exports.pullRequests_merge = function (req,res) {
        User.findById(req.body.id_user)
            .then((user) => {
                if (user) {
                    axios.put('https://api.github.com/repos/' + user.login + '/' + req.body.repo_name + '/pulls/' + req.body.pull_num + '/merge', {
                        commit_title: req.body.commit_title,
                        commit_message: req.body.commit_message,
                        sha: req.body.sha,
                        merge_method: req.body.merge_method
                    },{
                        headers: {
                            'accept': 'application/vnd.github.inertia-preview+json',
                            Authorization: `Bearer ${user.token_git}`}
                    })
                        .then(function (response) {
                            res.json({message: response.data.message, url: response.data.url});
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            });
    }
