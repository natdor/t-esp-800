const Comments = require('../models/commentsModel');
const User = require('../models/userModel');

exports.comments = function(req,res){
    Comments.find({design_id: req.params.design_id}, function(err,comments){

        let commentMap = {};
        comments.forEach(function (comment) {

            commentMap[comment._id.toString()] = comment;
        });
        res.send(commentMap);
    })
}


exports.comments_create = function (req,res) {

    User.findById( req.body.id_user).then((user)=>{
        const content   = req.body.content;
        const author    = user.login ;
        const design_id    = req.body.design_id ;
        const id_user   = user._id;
        const newComments =  new Comments({
            content: content,
            author: author,
            design_id:design_id,
            id_user: id_user});

        newComments.save().then((comment) =>{
            res.json({success: true, id: comment._id, content: comment.content,author:comment.author ,created_at: comment.create_date})
        });
    }).catch(function (error) {
        console.log(error);
    });

}


exports.comments_update = function (req,res) {
    User.findById( req.body.user_id).then((user)=>{
        const content       = req.body.content;
        const comment_id    =  req.body.comment_id;
        Comments.findByIdAndUpdate(comment_id,{content: content}, function(err, comment) {

            if (err) {
                res.send(err)
            } else {
                res.json({success: true, id: comment._id, content: comment.content,author:comment.author, updated_at: comment.create_date});
            }
        });

    }).catch(function (error) {
        console.log(error);
    });
}

exports.comments_delete = function (req,res) {
    Comments.findByIdAndDelete(req.params.id_comment,function(err, result) {

        if (err) {
            res.send(err)
        } else {
            res.json({status:"200", message:"your comment has been removed"})
        }
    });
}