const User = require('../models/userModel');
const axios = require('axios');
const moment = require('moment');
/**
 * List all repos
 *
 */


exports.repos = function(req,res){

    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                return axios.get('https://api.github.com/users/' + user.login + '/repos',{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                }).then(function (response) {

                        const data = [];
                        let i = 0;

                        response.data.forEach((repo) =>{
                            const dataObj = {};

                            dataObj['id'] = i ;
                            dataObj['name'] = repo.name;
                            dataObj['created_at'] = moment(new Date(repo.created_at)).format('MMMM Do YYYY');
                            dataObj['updated_at'] = moment(new Date(repo.updated_at)).format('MMMM Do YYYY');
                            dataObj['actions'] = {edit:'edit', delete:repo.name};
                            i++;
                            data.push(dataObj);
                        })

                        res.json(data);

                    }).catch(function (error) {
                        console.log(error);
                    });
            }
        })
}



/**
 * get one repo
 *
 */


exports.repo = function(req,res){

    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/users/' + user.login + '/'+req.params.repo_name,{
                    headers: {
                        'accept': 'application/vnd.github.nebula-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        res.json(response.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        })
}


/**
 *  Create a repo
 */

exports.repos_create = function (req,res) {
    User.findById(req.body.id_user)
        .then((user)=> {
            if (user) {

                axios.post('https://api.github.com/user/repos', {

                        name: req.body.name,
                        description: req.body.description,
                        homepage: req.body.homepage,
                        private: req.body.private,
                        has_issues: req.body.has_issues,
                        has_projects: req.body.has_projects,
                        has_wiki: req.body.has_wiki

                    }, {
                        headers: {
                            'Accept': 'application/vnd.github.nebula-preview+json',
                            Authorization: `Bearer ${user.token_git}`}
                    }
                ).then(function (response) {

                        console.log(response);
                            res.status(200).json({success: true, message: "A new repositori has been added"});


                    }).catch(function (error) {
                        console.log(error);

                    });

            }
        })
}

/**
 * Update a repo
 * @param req
 * @param res
 */
exports.repos_update = function (req,res) {
    User.findById(req.body.id_user)
        .then((user)=> {
            axios.patch('https://api.github.com/repos/' + user.login + '/' + req.body.repo_name, {
                name: req.body.name,
                description: req.body.description,
                homepage: req.body.homepage,
                private: req.body.private,
                has_issues: req.body.has_issues,
                has_projects: req.body.has_projects,
                has_wiki: req.body.has_wiki
            }, {
                headers: {
                    'accept': 'application/vnd.github.inertia-preview+json',
                    Authorization: `Bearer ${user.token_git}`}
            })
                .then(function (response) {
                    if(user){
                            res.json({success: true, message: "A new repositori has been added"});
                    }

                })
                .catch(function (error) {
                    console.log(error);
                });
        })
}
/**
 * Delete a repo
 * @param req
 * @param res
 */
exports.repos_delete = function (req,res) {
    User.findById(req.params.id_user)
        .then((user) => {
            axios.delete('https://api.github.com/repos/' + user.login + '/' + req.params.repo_name, {
                headers: {
                    'accept': 'application/vnd.github.inertia-preview+json',
                    Authorization: `Bearer ${user.token_git}`
                }
            }).then(function (response) {
                res.json(response.data);
            }).catch(function (error) {
                    console.log(error);
                });
        })
}

    /**
     * Repo files update or create
     */

    exports.repos_file_update_or_create = function (req,res) {
        User.findById(req.body.id_user)
            .then((user)=> {
                let content = req.body.content;
                let buff = new Buffer(content);
                let content_64 = buff.toString('base64');
                axios.put('https://api.github.com/repos/'+user.login+'/'+req.body.repo_name+'/contents/'+req.body.path, {
                    message: req.body.message,
                    content: content_64,
                    sha: req.body.sha,
                    branch: req.body.branch,
                    committer: req.body.committer,
                    author: req.body.author
                }, {
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                }).then(function (response) {
                        if(user){
                            res.json({success: true, message: "A new file has been added or updated"});
                        }

                    }).catch(function (error) {
                        console.log(error);
                    });
            });
    }

exports.repos_files = function(req,res){

    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/repos/'+user.login+'/'+req.params.repo_name+'/contents/',{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        res.json(response.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        })
}

    exports.repos_file = function(req,res){

    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/repos/'+user.login+'/'+req.params.repo_name+'/contents/'+req.params.path,{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        res.json(response.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        })
}

exports.repos_file_delete = function (req,res) {
    User.findById(req.params.id_user)
        .then((user) => {
            axios.delete('https://api.github.com/repos/'+user.login+'/'+req.params.repo_name+'/contents/'+req.params.path, {
                headers: {
                    'accept': 'application/vnd.github.inertia-preview+json',
                    Authorization: `Bearer ${user.token_git}`
                }
            }).then(function (response) {
                res.json(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        })
}