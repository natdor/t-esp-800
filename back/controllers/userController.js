const utils = require('../lib/utils');
const User = require('../models/userModel');

exports.users = function(req,res,next){

   User.find({}, function(err,users){
       //console.log(users);
       let userMap = {};
       users.forEach(function (user) {
            //console.log(user._id);
            userMap[user._id.toString()] = user;
       });
       res.send(userMap);
   })
}


exports.users_create = function (req,res,next) {
    const saltHash = utils.genPassword(req.body.password);
    const salt = saltHash.salt;
    const hash = saltHash.hash;
    const login = req.body.login;
    const token_git= req.body.token_git;
    const email = req.body.email;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const newUser = new User({
        login:login,
        email:email,
        firstname:firstname,
        lastname:lastname,
        hash: hash,
        salt: salt,
        token_git:token_git
    });

    newUser.save()
        .then((user) =>{
            const jwt = utils.issueJWT(user);
            res.json({success: true, id: user._id,token_git: user.token_git, token: jwt.token, expiresIn: jwt.expires});
        }).catch(err => next(err));

}


exports.users_login = function (req,res,next) {

    User.findOne({email: req.body.email})
        .then((user)=> {
           // console.log(user);
            if(!user){
                res.status(401).json({success: false, msg:"could not find user"});
            }


            const isvalid = utils.validPassword(req.body.password, user.hash, user.salt);

            if (isvalid){
                const tokenObject = utils.issueJWT(user);
                res.status(200).json({success: true, _id: user._id,token_git: user.token_git,login: user.login,token: tokenObject.token, expiresIn: tokenObject.expires});

            } else{
                res.status(401).json({success: false, msg: "you entrered the wrong password"});
            }
        })

}


exports.users_profile = function (req,res) {

    User.findById(req.params.id_user)
        .then((user)=> {
            if(user){
            res.json({user_id: user._id, login: user.login, email: user.email, name: user.name, firstname: user.firstname, role: user.role})
        }else{
                res.json({status: 200, message:"this user doesn't exist"});
            }
            }).catch(function (error) {
        console.log(error);
    });
}

exports.users_update = function (req,res) {

    const saltHash = utils.genPassword(req.body.password);
    const salt = saltHash.salt;
    const hash = saltHash.hash;
    const login = req.body.login;
    const token_git= req.body.token_git;
    const email = req.body.email;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
User.findByIdAndUpdate(req.body.user_id,{
        login:login,
        email:email,
        firstname:firstname,
        lastname:lastname,
        hash: hash,
        salt: salt,
        token_git:token_git
    }, function(err, result) {

    if (err) {
        res.send(err)
    } else {
        res.send(result)
    }

});
}

exports.users_delete = function (req,res) {
   User.findByIdAndDelete(req.params.user_id,function(err, result) {

       if (err) {
           res.send(err)
       } else {
           res.send(result)
       }
    })

}