const Kanban = require('../models/kanbansModel');

exports.kanbans = async function(req,res) {
    try {
        const kanbans = await Kanban.find();
        res.json(kanbans);
    } catch (err) {
        res.status(400).send(err);
    }
}

exports.find_kanban = async function(req,res) {
    try {
        const kanban = await Kanban.findById(req.params.kanbanId);
        res.json(kanban);
    } catch (err) {
        res.status(400).send(err);
    }
}

exports.create_kanban = function(req,res) {
    const newKanban = new Kanban({
        title: req.body.title, 
        description: req.body.description,
        user_id: req.params.userId,
        lanes: req.body.lanes
    });

    newKanban.save().then((kanban)=>{
        res.status(201).json({id:kanban._id, created_at:kanban.created_at, msg:"the kanban has been created", created: true});
    })
}

exports.update_kanban = async function(req,res) {
    try {
        const updateKanban = await Kanban.updateOne(
            {_id: req.params.kanbanId},
            {$set: {title: req.body.title, description: req.body.description}}
        )
        res.json(updateKanban);
    } catch (err) {
        res.status(400).send(err);
    }
}

exports.delete_kanban = async function(req,res) {
    try {
        const removedKanban = await Kanban.remove({_id: req.params.kanbanId})
        res.json(removedKanban);
    } catch {
        res.status(400).send(err);
    }
}