const User = require('../models/userModel');
const axios = require('axios');

/**
 * List all collaborators
 * @param req
 * @param res
 */
exports.collaborators = function(req,res){
    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/repos/'+user.login+'/'+req.params.repo_name+'/collaborators',{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                }).then(function (response) {
                        res.json(response.data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
            })
}

/**
 * Add collaborators
 * @param req
 * @param res
 */
exports.collaborators_add = function (req,res) {
    User.findById(req.body.id_user)
        .then((user)=> {
            if (user) {
 axios.put( 'https://api.github.com/repos/'+user.login+'/'+req.body.repo_name+'/collaborators/'+req.body.username, {
  permission: req.body.permission
 },{
     headers: {
         'accept': 'application/vnd.github.inertia-preview+json',
         Authorization: `Bearer ${user.token_git}`}
 })
     .then(function (response) {
         res.json(response);
     })
     .catch(function (error) {
      console.log(error);
     });
            }
        })
}



/**
 * Delete collaborators
 * @param req
 * @param res
 */
exports.collaborators_delete = function (req,res) {
    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
    axios.delete( 'https://api.github.com/repos/'+user.login+'/'+req.params.repo_name+'/collaborators/'+req.params.username, {
        headers: {
            'accept': 'application/vnd.github.inertia-preview+json',
            Authorization: `Bearer ${user.token_git}`}
    })
        .then(function (response) {
            res.json(response);
        })
        .catch(function (error) {
            console.log(error);
        });
            }
        })
}