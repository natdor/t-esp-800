const Designs = require('../models/designsModel');
const Comments = require('../models/commentsModel');
const User = require('../models/userModel');

exports.designs = function(req,res){
    Designs.find({}, function(err,designs){
        //console.log(users);
        let designMap = {};
        designs.forEach(function (design) {
            //console.log(user._id);
            designMap[design._id.toString()] = design;
        });
        res.send(designMap);
    })
}


exports.design = function(req,res){
    Designs.findById(req.params.id_design).then((design)=>{
        if(design){
            res.json({success: true, id: design._id, url: design.design_url, created_at: design.create_date});
        }else{
            res.json({status: 200, message:"the design doesn't exist"});
        }
    }).catch(function (error) {
        console.log(error);
    });

}

exports.designs_create = function (req,res) {
    User.findById( req.body.id_user).then((user)=>{
        const name      = req.body.name;
        const design_url= req.body.design_url ;
        const user_id   = user._id;
        const newDesign =  new Designs({
            name: name,
            design_url: design_url,
            user_id: user_id});

        newDesign.save().then((design) =>{
           res.json({success: true, id: design._id, url: design.design_url, created_at: design.create_date})
        });
    }).catch(function (error) {
        console.log(error);
    });

}


exports.designs_update = function (req,res) {

    User.findById( req.body.user_id).then((user)=>{
        const name      = req.body.name;
        const design_url= req.body.design_url;
        const user_id   = user._id;
        const design_id =  req.body.design_id;
        Designs.findByIdAndUpdate(design_id,{
            name: name,
            design_url: design_url,
            user_id: user_id}, function(err, design) {

            if (err) {
                res.send(err)
            } else {
                res.json({success: true, id: design._id, url: design.design_url, created_at: design.create_date});
            }
        });

    }).catch(function (error) {
        console.log(error);
    });

}

exports.designs_delete = function (req,res) {
    Comments.findOneAndDelete({design_id:req.params.id_design},function(err, result) {

        if (err) {
            res.send(err)
        } else {
            res.send(result)
        }
    });
    Designs.findByIdAndDelete(req.params.id_design,function(err, result) {

        if (err) {
            res.send(err)
        } else {
            res.send(result)
        }
    });
}