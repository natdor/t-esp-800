const Team = require('../models/teamsModel');


exports.teams_create = function(req,res){


    const name = req.body.name;
    const members = req.body.members;
    const user_id = req.body.user_id;

    const newTeam = new Team({
        name: name,
        members: members,
        user_id: user_id
    });

    newTeam.save().then((team)=>{
        res.status(201).json({id:team._id, created_at:team.created_at, msg:"the team has been created", created: true});
    })

}

exports.teams = function(req,res){

    Team.find({}, function(err,teams){
        let teamMap = {};
        teams.forEach(function(team,index){
            teamMap[index] = team;
        })
        res.status(200).json(teamMap);
    })

}


exports.teams_update = function(req,res){


    Team.findById(req.id,function (err, team) {

        const name = req.body.name != team.name ? req.body.name : team.name;
        const members = req.body.members != team.members ? req.body.members : team.members;
        const updated_at = new Date() ;

        const updateTeam = new Team({
            name: name,
            members: members,
            updated_date: updated_at
        });
        Team.findByIdAndUpdate(req.id,updateTeam, (err,team)=>{
            if(err){
                console.log(err);
            }else{
                res.status(204).json({id:team._id, msg:'the team has been updater', updated_at: team.updated_at, updated: true})
            }
        })
    })


}

exports.teams_delete = function(req,res){
    Team.findByIdAndDelete(req.params.id,function (err, team) {

        if(err){
            res.status(400).send(err)
        }else{
            res.status(204).json({id: team._id, deleted: true});
        }
    })
}