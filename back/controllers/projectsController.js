const User = require('../models/userModel');
const Project = require('../models/projectsModel');
const axios = require('axios');
const moment = require('moment');




exports.projects = function(req,res){
    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/repos/'+user.login+'/'+req.params.repo_name+'/projects',{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        const data = [];
                        let i = 0;
                        console.log(response);
                        response.data.forEach((project) =>{
                            const dataObj = {};

                            dataObj['id'] = i ;
                            dataObj['name'] = project.name;
                            dataObj['created_at'] = moment(new Date(project.created_at)).format('MMMM Do YYYY');
                            dataObj['updated_at'] = moment(new Date(project.updated_at)).format('MMMM Do YYYY');
                            dataObj['actions'] = {view: '/kanban/'+project.id, edit:'edit', delete:'delete'};
                            i++;
                            data.push(dataObj);
                        })
                        res.json(data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}

exports.project = function(req,res){
    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/projects/'+req.params.project_id, {

                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        res.json([{url:response.data.html_url}]);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}

exports.projects_create = function (req,res) {


    User.findById(req.body.id_user)
        .then((user)=> {
            if (user) {
                axios.post('https://api.github.com/repos/'+user.login+'/'+req.body.repo_name+'/projects', {

                        name: req.body.name,
                        body: req.body.description

                    }, {
                        headers: {
                            'accept': 'application/vnd.github.inertia-preview+json',
                            Authorization: `Bearer ${user.token_git}`}
                    }
                )
                    .then(function (response) {

                        let newProject = {
                            id:"",
                            name:"",
                            body:""

                        };
                            res.json({success: true, message: "A new project has been added"});


                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}


exports.projects_update = function (req,res) {
    User.findById(req.body.id_user)
        .then((user)=> {
            if (user) {
                axios.patch('https://api.github.com/projects/'+req.body.project_id, {

                        name: req.body.name,
                        body: req.body.description

                    }, {
                        headers: {
                            'accept': 'application/vnd.github.inertia-preview+json',
                            Authorization: `Bearer ${user.token_git}`}
                    }
                )
                    .then(function (response) {

                            res.json({success: true, message: "the project has been updated"});


                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}

exports.projects_delete = function (req,res) {
    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.delete('https://api.github.com/projects/' + req.params.project_id , {
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                }).then(function (response) {
                    res.json(response.data);
                })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}

exports.columns = function(req,res){

    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/projects/'+req.params.project_id+'/columns',{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                      //  console.log(response);
                        const data = new Array();

                        let i = 0;
                        response.data.forEach((column) =>{
                            const dataObj = {};

                            dataObj['id'] = 'lane'+column.id;
                            dataObj['title'] = column.name;
                            i++;
                            data.push(dataObj);
                        })

                        res.json(data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}



exports.cards = function(req,res){

    User.findById(req.params.id_user)
        .then((user)=> {
            if (user) {
                axios.get('https://api.github.com/projects/columns/'+req.params.column_id+'/cards',{
                    headers: {
                        'accept': 'application/vnd.github.inertia-preview+json',
                        Authorization: `Bearer ${user.token_git}`}
                })
                    .then(function (response) {
                        console.log(response);
                        const data = new Array();

                        let i = 0;
                        response.data.forEach((card) =>{
                            const dataObj = {};

                            dataObj['id'] = 'Card'+card.id;
                            dataObj['title'] = card.note;
                            dataObj['description'] = card.note;
                            i++;
                            data.push(dataObj);
                        })

                        res.json(data);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
}
