import React  from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import  { Redirect } from 'react-router-dom';
import {Alert} from "reactstrap";
import logo from '../../images/git-logo.png';
import './Login.css'
import axios from 'axios';

export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      loginError: "",
      toDashboard: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {

    const {email, password} = this.state;
    axios.post('http://localhost:8000/user/login', {
        email: email,
        password: password,
      }).then( ( response ) => {
          if (response.statusText === "OK") {
              localStorage.setItem('UserId', response.data._id);
              localStorage.setItem('UserToken', response.data.token);
              localStorage.setItem('name', response.data.login);
              this.setState({toDashboard: true});
          }
      }).catch( ( error ) => {
        this.setState({loginError: <Alert color="danger">Incorect email adress or password.</Alert>})
      }).finally(function(){})
      event.preventDefault();

  }

  render() {

    if (this.state.toDashboard === true) {
      this.setState({loginError: ""})

      return <Redirect to='/my-projects'/>;
    }

    else {
            return (
              <div className="login-container">
                <form onSubmit={this.handleSubmit} className="app-form">
                    {this.state.loginError}
                    <h4 className="signup-form-title">Sign-in</h4>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email"
                        autoComplete="email"
                        type="email" 
                        name="email"
                        autoFocus
                        value={this.state.email}
                        onChange={this.handleChange}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        value={this.state.password}
                        onChange={this.handleChange}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        value="Submit"
                    >
            Sign In
                    </Button>
                    <Grid container>
                        <Grid item>
                            <Link id="signup" href="/sign-up" variant="body2">
                                {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid>
                    <div className="col-12 git-connect"><img className="img-fluid git-logo"  src={logo} alt=""/>
                    <a href="http://localhost:8000/user/auth/github">Github Connect</a></div>

                </form>
              </div>
        );
      }
    }
}