import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/Menus.css';


export default function SearchBar() {

    const logout = data => {
        localStorage.setItem('UserId', '');
        localStorage.setItem('UserToken', '');
        localStorage.setItem('name', '');
        window.location.href = "/login";
    }
    return (
        <div>
            <div className="menu-top">
                <div className="container">
                    <ul className="d-flex justify-content-end mb-0">
                        <li><a href="#" onClick={logout}>Logout</a></li>
                    </ul>
                </div>
            </div>
        <div className="search-bar">
            <input type="text"/>
        </div>
        {/*
            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light">
                    <a className="navbar-brand" href="/my-projects">Logo</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav main-menu">
                            <li className="nav-item">
                                <a className="nav-link" href="/my-projects">My Projects</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/manage-projects">Manage Projects</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/my-teams">My Teams</a>
                            </li>
                        </ul>
                    </div>
                </nav>
        </div>*/}
        </div>
    );
}