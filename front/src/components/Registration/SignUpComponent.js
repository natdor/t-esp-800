import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/form.css';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import './SignUp.css'

export default class SignUpComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: "",
            login: "",
            firstname: "",
            lastname: "",
            password: "",
            confirmedPassword: "",
            token_git:"",
            loginErrors: "",
            redirect: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {

        const {login, email, firstname, lastname, password,token_git} = this.state;
        axios.post('http://localhost:8000/user/signup', {
            login: login,
            email: email,
            firstname: firstname,
            lastname: lastname,
            password: password,
            token_git:token_git
        }).then( ( response ) => {
            if (response.statusText === "OK") {
                localStorage.setItem('UserId', response.data.id);
                localStorage.setItem('UserToken', response.data.token);
                this.Redirection();
            }
        })

            .catch(function(error){
            }).finally(function(){})
        event.preventDefault();

    }

    Redirection(){
        this.setState({redirect: true});
    }

    render(){

        if (this.state.redirect === true)
            return <Redirect to='/my-repositories'/>;

        return(<div className="container">
                        <form onSubmit={this.handleSubmit} className="app-form">
                            <h4 className="signup-form-title">Sign-up</h4>
                            <div className="form-group">
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    label="Lastname"
                                    autoComplete="lastname"
                                    type="name"
                                    name="lastname"
                                    className="form-control"
                                    value={this.state.lastname}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="form-group">
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    type="name"
                                    name="firstname"
                                    className="form-control"
                                    label="Firstname"
                                    autoComplete="Firstname"
                                    value={this.state.firstname}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="form-group">
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    type="email"
                                    name="email"
                                    className="form-control"
                                    label="Email"
                                    autoComplete="Email"
                                    value={this.state.email}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="form-group">
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    type="text"
                                    name="login"
                                    className="form-control"
                                    label="Github Login"
                                    autoComplete="Github Login"
                                    value={this.state.login}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="form-group">
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    type="text"
                                    name="token_git"
                                    className="form-control"
                                    label="Github Token"
                                    autoComplete="Github Token"
                                    value={this.state.token_git}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="register-form-footer">
                                <div className="form-group">
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        type="password"
                                        name="password"
                                        className="form-control"
                                        label="Password"
                                        autoComplete="Password"
                                        value={this.state.password}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        type="password"
                                        name="confirmedPassword"
                                        className="form-control"
                                        label="Confirmed Password"
                                        autoComplete="Confirmed Password"
                                        value={this.state.confirmedPassword}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </div>
                            <div className="form-footer-group">
                                <button
                                    className="btn btn-primary form-button"
                                    type="submit">
                                    Submit
                                </button>
                            </div>
                            <p className="register-form-text">
                                <a className="sign-up-button" href="/login">Back to Login</a>
                            </p>
                        </form>
        </div>);
    }
}