import React, {Component} from 'react';
import axios from "axios";
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import paginationFactory, { PaginationProvider,PaginationListStandalone,
    PaginationTotalStandalone,
    SizePerPageDropdownStandalone } from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
/*import {Link} from "react-router-dom";*/
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/Table.css';
const { SearchBar } = Search;


export default class MyProjectsComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            description: "",
            homepage: "",
            private_: false,
            has_issues: true,
            has_projects: true,
            has_wiki: true,
            selected: [],
            name_selected:[],


            repos: [],

            columns:[{
                    dataField: 'name',
                    text: 'name'
                },{
                    dataField: 'created_at',
                    text: 'created at'
                },{
                    dataField: 'updated_at',
                    text: 'updated at'
                },
                {
                    dataField:'actions',
                    text: 'Actions',
                    formatter: this.actionFormatter
                }
            ]
        }

        this.kanbanRef = React.createRef();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBtnClick = this.handleBtnClick.bind(this);
        //this.actionFormatter = this.actionFormatter.bind(this);
        this.handleBtnEdit = this.handleBtnEdit.bind(this);

    }

   /* checkFormatter(cell,row){
        return(<input type="checkbox"/>)

    }*/

    actionFormatter = (cell, row) => {

        return (
            <span>
                <a href={ cell.edit} style={{paddingRight:"5px"}} onClick={this.handleBtnEdit}>
                    {/*<FontAwesomeIcon icon={faPen}/>*/}
                    Edit
                </a>

                <a href="#" data-delete={cell.delete} onClick={this.handleBtnDelete}>
                    {/*<FontAwesomeIcon icon={faTrash}/>*/}
                    Delete
                </a>
            </span>);

    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }


    handleSubmit = (event) => {
        event.preventDefault();
        let user_id = localStorage.getItem('UserId');
        const {name, description, homepage, private_, has_issues, has_projects, has_wiki } = this.state;
        axios.post('http://localhost:8000/add/repos', {
            id_user: user_id,
            name: name ,
            description: description,
            homepage: homepage ,
            private: private_ ,
            has_issues: has_issues,
            has_projects: has_projects,
            has_wiki: has_wiki
        },{headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {
            document.getElementById('addRepo').classList.remove('show');
            document.querySelector('.modal-backdrop').classList.remove('show');
            //$('#addRepo').modal('hide');
            console.log(response);
        }).catch(function(error){
            }).finally(function(){})


    }

    componentDidMount() {
        console.log(this.props.match);
        const queryString = window.location.search;
        if (queryString.length > 0){
            const urlParams = new URLSearchParams(queryString);
            const token = urlParams.get('token');
            const id_user = urlParams.get('id_user');
            const login = urlParams.get('login');
            localStorage.setItem('UserId', id_user);
            localStorage.setItem('UserToken', 'Bearer '+token);
            localStorage.setItem('name', login);
        }
        let user_id = localStorage.getItem('UserId');
        axios.get('http://localhost:8000/list/'+user_id+'/repos', {headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then((res) => {
            console.log(res);
            this.setState({repos:res.data})
        });
    }


    handleBtnClick = (event) => {
        event.preventDefault();
        const {name, description,name_selected} = this.state;
        let user_id = localStorage.getItem('UserId');
        console.log("clicked");
        axios.post('http://localhost:8000/add/projects', {
            id_user: user_id,
            repo_name: name_selected[0].name,
            name: name,
            description: description

        },{headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {

            document.getElementById('createProject').classList.remove('show');
            document.querySelector('modal-backdrop').classList.remove('show');
            console.log(response);
        }).catch(function(error){
        }).finally(function(){})

    }

    handleBtnDelete = (event) => {
        event.preventDefault();

        let user_id = localStorage.getItem('UserId');
        let repo_name = event.target.dataset.delete;

        console.log(repo_name);
        axios.delete('http://localhost:8000/delete/'+user_id+'/repos/'+repo_name,{headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {

        console.log(response);
        }).catch(function(error){
        }).finally(function(){})

    }

    handleBtnEdit = (event) => {
        event.preventDefault();
        let user_id = localStorage.getItem('UserId');
        let repo_name = this.state[0].name;
        const {name, description, homepage, private_, has_issues, has_projects, has_wiki } = this.state;
        console.log();
        axios.patch('localhost:8000/patch/'+user_id+'/'+repo_name,{
            id_user: user_id,
            name: name ,
            description: description,
            homepage: homepage ,
            private: private_ ,
            has_issues: has_issues,
            has_projects: has_projects,
            has_wiki: has_wiki
        
        },{headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {

        console.log(response);
        }).catch(function(error){
        }).finally(function(){})

    }

    handleOnSelect = (row, isSelect) => {
        if (isSelect) {


            this.setState(() => ({
                selected: [...this.state.selected, row.id],
                name_selected: [{name : row.name}]
            }));
        } else {
            this.setState(() => ({
                selected: this.state.selected.filter(x => x !== row.id)
            }));
        }
    }

    handleOnSelectAll = (isSelect, rows) => {
        const ids = rows.map(r => r.name);
        if (isSelect) {
            this.setState(() => ({
                selected: ids
            }));
        } else {
            this.setState(() => ({
                selected: []
            }));
        }
    }




    render() {
        {
            const paginationOption = {
                sizePerPage: 5,
                custom: true,
                totalSize: this.state.repos.length
            };

            const selectRow = {
                mode: 'checkbox',
                clickToSelect: true,
                selected: this.state.selected,
                onSelect: this.handleOnSelect,
                onSelectAll: this.handleOnSelectAll
            };

            return (<div className="container-component" >
                <ToolkitProvider keyField="id" data={this.state.repos} columns={this.state.columns} search>
                    {
                        props =>

                            <PaginationProvider
                                pagination={paginationFactory(paginationOption)}
                            >
                                {
                                    ({
                                         paginationProps,
                                         paginationTableProps
                                     }) => (
                                        <div className="containerIn">

                                            <div className="title-wrapper d-flex justify-content-between align-items-center"><h2>My project</h2> <span className="btn btn-form" data-toggle="modal" data-target="#addRepo">Add</span></div>
                                            <div className="search-container d-flex align-items-center justify-content-between">
                                                <span className="btn btn-form" data-toggle="modal"  data-target="#createProject">Create Kanban</span>
                                                <span className="item-search">
                                                <SearchBar className="edit-user-tab-search" {...props.searchProps} />
                                            </span>
                                            </div>
                                            <BootstrapTable keyField="id" headerClasses={"text-center"}
                                                            rowStyle={{textAlign: "center"}} {...props.baseProps}
                                                            striped={true} hover={true} {...paginationTableProps} selectRow={selectRow}/>
                                            <SizePerPageDropdownStandalone {...paginationProps} />
                                            <PaginationTotalStandalone {...paginationProps}/>
                                            <PaginationListStandalone {...paginationProps}/>
                                        </div>
                                    )
                                }
                            </PaginationProvider>

                    }
                </ToolkitProvider>
                <div className="modal fade" id="createProject" ref={this.kanbanRef} tabIndex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden={true}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">New Kanban</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden={true}>&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form onSubmit={this.handleBtnClick }>
                                    <div className="form-group">
                                        <label className="col-form-label">Name:</label>
                                        <input name="name" type="text" className="form-control" id="name" onChange={this.handleChange} value={this.state.name}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="message-text" className="col-form-label">Description:</label>
                                        <textarea name="description" className="form-control" id="message-text" onChange={this.handleChange} value={this.state.description}></textarea>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" className="btn btn-form">Create Kanban</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="addRepo" tabIndex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden={true}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">New project</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden={true}>&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        <label className="col-form-label">Name:</label>
                                        <input name="name" type="text" className="form-control" id="name" onChange={this.handleChange} value={this.state.name}/>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col-form-label">Homepage:</label>
                                        <input name="homepage" type="text" className="form-control" id="homepage" onChange={this.handleChange} value={this.state.homepage}/>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col-form-label">Private:</label>
                                        <select name="private_" className="form-control" id="private" onChange={this.handleChange} value={this.state.private_}>
                                            <option value={false} defaultValue>false</option>
                                            <option value={true}>true</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col-form-label">Has projects:</label>
                                        <select name="has_projects" className="form-control" id="has-projects" onChange={this.handleChange} value={this.state.has_projects}>
                                            <option value={false} defaultValue>false</option>
                                            <option value={true}>true</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col-form-label">Has issues:</label>
                                        <select name="has_issues" className="form-control" id="has-issues" onChange={this.handleChange} value={this.state.has_issues}>
                                            <option value={false} defaultValue>false</option>
                                            <option value={true}>true</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col-form-label">Has wiki:</label>
                                        <select name="has_wiki" className="form-control" id="has-wiki" onChange={this.handleChange} value={this.state.has_wiki}>
                                            <option value={false} defaultValue>false</option>
                                            <option value={true}>true</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="message-text" className="col-form-label">Description:</label>
                                        <textarea name="description" className="form-control" id="message-text" onChange={this.handleChange} value={this.state.description}></textarea>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" className="btn btn-form">Create Project</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>);

        }
    }
}