import React, { useState, useEffect } from 'react';
import Team from './Team/Team';
import './Teams.css';
import axios from 'axios';



const Teams = () => {
    const [teams, setTeams] = useState([]);
    const [name,setName] = useState("");
    const [members,setMembers] = useState([]);



    const handleBtnClick = (event) => {
        event.preventDefault();

        let user_id = localStorage.getItem('UserId');
        console.log(name);

        axios.post('http://localhost:8000/add/teams', {
            name: name,
            members: members,
            user_id: user_id
        },{headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {


            document.getElementById('chat').classList.remove('show');
            document.querySelector('modal-backdrop').classList.remove('show');
            console.log(response);
        }).catch(function(error){
        }).finally(function(){})

    }

    useEffect(()=>{
        axios.get('http://localhost:8000/teams',{headers: {'Authorization': `${localStorage.getItem('UserToken')}`}}).then(function (res) {

            const data = []

            for (const team in res.data){
                data.push(res.data[team])
            }
            setTeams(data);



        })
    })

    return(<div className="col-11">
        <div className="title-wrapper d-flex justify-content-between align-items-center"><h2>My teams</h2> <span className="btn btn-form" data-toggle="modal" data-target="#chat">Add</span></div>
        <div>
            <div className="row card-container">
            {teams.map((team,i)=><div className="card col-md-2" key={i}><Team  members={team.members} room={team.name} idTeam={team._id}/></div>)}
            </div>
            <div className="modal fade" id="chat"  tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden={true}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">New Chat</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden={true}>&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={handleBtnClick }>
                                <div className="form-group">
                                    <label className="col-form-label">Name:</label>
                                    <input name="name" type="text" className="form-control" id="name" onChange={({ target: { value } }) =>{setName(value)}} />
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" className="btn btn-form">Create Chat</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>)
}

export default Teams;