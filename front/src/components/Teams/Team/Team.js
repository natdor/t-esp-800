import React, { useState} from 'react';
import './Team.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faTrash} from '@fortawesome/free-solid-svg-icons';
import axios from "axios";

const Team = ({room, idTeam}) => {

    const trashClick = (event) =>{
        let id = event.currentTarget.dataset.id;

        axios.delete('http://localhost:8000/teams/'+id+'/delete',{headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {
            event.currentTarget.parentNode.parentNode.parentNode.style.display = 'none'

        }).catch(function(error){
        }).finally(function(){})
    }
    return (<div className="card-body">
        <div className="delete-wrapper d-flex justify-content-end"><span  onClick={trashClick} data-id={idTeam} className="delete"><FontAwesomeIcon icon={faTrash}/></span></div>
        <a className="linkChat" href={'/chat?name='+localStorage.getItem('name')+'&room='+room+'&team_id='+idTeam} >
                            <h5 className="card-title">{room}</h5>
                            </a>
                        </div>);

}

export default Team;