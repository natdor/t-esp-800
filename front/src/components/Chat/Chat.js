import React, { useState, useEffect } from "react";
import queryString from 'query-string';
import io from "socket.io-client";

import TextContainer from '../TextContainer/TextContainer';
import Messages from '../Messages/Messages';
import InfoBar from '../InfoBar/InfoBar';
import Input from '../Input/Input';
import Peer from "simple-peer";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone } from '@fortawesome/free-solid-svg-icons';

import './Chat.css';

const ENDPOINT = 'localhost:8000';

let socket;

const Chat = ({ location }) => {
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [users, setUsers] = useState('');
    const [message, setMessage] = useState('');
    const [team_id, setTeamId] = useState('');
    const [messages, setMessages] = useState([]);
    const [stream, setStream] = useState();
    const [receivingCall, setReceivingCall] = useState(false);
    const [caller, setCaller] = useState("");
    const [callerSignal, setCallerSignal] = useState();
    const [callAccepted, setCallAccepted] = useState(false);

    useEffect(() => {
        const { name, room, team_id } = queryString.parse(location.search);

        socket = io(ENDPOINT);

        setRoom(room);
        setName(name);
        setTeamId(team_id);
        socket.emit('join', { name, room,team_id }, (error) => {
            if(error) {
                alert(error);
            }
        });
    }, [ENDPOINT, location.search]);

    useEffect(() => {
        socket.on('message', message => {

            setMessages(messages => [ ...messages, message ]);
        });

        socket.on("roomData", ({ users }) => {
            setUsers(users);
        });
    }, []);

    const sendMessage = (event) => {
        event.preventDefault();

        if(message) {
            socket.emit('sendMessage', message,team_id,() => setMessage(''));
        }
    }

    return (
        <div className="col-11 chat-wrapper p-0">
            <div className="row chat-In m-0">
                <div className="col-md-4 p-0">
                    <div className="call-title"><span>Call a member</span></div>
                    <div className="list-group">
                        {users ? users.map((user,i) => (<div className="item-wrapper list-group-item list-group-item-secondary" data-socket-id={user.id} key={i}><span>{user.name}</span><span className="phone-icon"><FontAwesomeIcon icon={faPhone}/></span></div>)):""}
                    </div>
                </div>
                <div className="col-md-8 p-0">
                    <InfoBar room={room} users={users} />
                    <Messages messages={messages} name={name} team_id={team_id}/>
                    <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
                </div>
            </div>
            </div>

    );
}

export default Chat;