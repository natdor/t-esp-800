import React from 'react';

import onlineIcon from '../../icons/onlineIcon.png';
import closeIcon from '../../icons/closeIcon.png';

import './InfoBar.css';

const InfoBar = ({ room}) => (
    <div className="infoBar">
        <div className="leftInnerContainer">
            <img className="onlineIcon" src={onlineIcon} alt="online icon" />
            <h3 className="m-0">{room}</h3>

        </div>
        <div className="rightInnerContainer">
            <a href="/teams"><img src={closeIcon} alt="close icon" /></a>
        </div>
    </div>
);

export default InfoBar;