import React,{Component} from "react";
import Board from 'react-trello'
import axios from "axios";
import { ListGroup, ListGroupItem } from 'reactstrap';
import './Kanban.css'

export default class Kanban extends Component {

    constructor(props) {
        super(props);
        this.state = {
          userInfos: [],
          kanbans: [],
          userKanbans: [],
          lanes: [
            {
              id: 'lane1',
              title: 'Planned Tasks',
              label: '2/2',
              cards: [
                {id: 'Card1', title: 'Write Blog', description: 'Can AI make memes', label: '30 mins'},
                {id: 'Card2', title: 'Pay Rent', description: 'Transfer via NEFT', label: '5 mins'}
              ]
            },
            {
              id: 'lane2',
              title: 'Completed',
              label: '0/0',
              cards: []
            }
          ]
        }

        this.CreateKanban = this.CreateKanban.bind(this);
        this.ListUserKanbans = this.ListKanbans.bind(this);
    }

    get_user_id() {
      axios.get('http://localhost:8000/users', {headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {
          if (response.statusText === "OK")
              this.setState({userInfos: response.data});
      }).catch( ( error ) => {
      })
    }

    componentDidMount() {
      this.ListUserKanbans()
   }

   CreateKanban() {
     const {lanes} = this.state;
     axios.post('http://localhost:8000/kanban/create/' + localStorage.getItem('UserId'), 
     {
      title: "kanban-test",
      description: "kanban-description",
      lanes: lanes
    },
     {headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {
          if (response.statusText === "OK")
            console.log("kanban created");
      }).catch( ( error ) => {
        return <alert>Problem with kanban creation</alert>
      })
   }

   DeleteKanban(kanban_id) {

    axios.delete('http://localhost:8000/kanban/delete/' + kanban_id, 
    {headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {
         if (response.statusText === "OK")
          console.log("kanban deleted");
     }).catch( ( error ) => {
       return <alert>Problem with kanban creation</alert>
     })
  }

   selectUserKanbans() {
     for (let i = 0; this.state.kanbans[i]; i++)
      if (this.state.kanbans[i].user_id === localStorage.getItem('UserId')) {
        for (let j = 0; this.state.userKanbans[j]; j++)
         if (this.state.userKanbans[j]._id === this.state.kanbans[i]._id)
          i++;
        this.state.userKanbans.push(this.state.kanbans[i])
      }
   }

   ListKanbans() {

    axios.get('http://localhost:8000/kanbans', {headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {
        if (response.statusText === "OK") {
          this.setState({kanbans: response.data});
        }
      }).catch( ( error ) => {
    })
   }

    render() {

        this.selectUserKanbans();

        return (
          <div className="kanbans-list">
            <button className="kanbans-list-buttons btn btn-form" onClick={this.CreateKanban}>create new kanban</button>
            <React.Fragment>
              <ListGroup className="list-group">
                {this.state.userKanbans.map(userKanban => (
                  <ListGroupItem action className="kanban-list-element" key={userKanban._id}>
                    <p className="kanban-title-list">{userKanban.title}</p>
                    <p className="kanban-description-list">{userKanban.description}</p>
                    <button className="kanbans-list-buttons btn-outline btn-form">delete kanban</button>
                    <button className="kanbans-list-buttons btn-outline btn-form" style={{marginLeft: '1.5rem'}}>invite teammate</button>
                  </ListGroupItem>
                ))}
              </ListGroup>
            </React.Fragment>
          </div>
        );

        /*
        const data = {
            lanes: [
              {
                id: 'lane1',
                title: 'Planned Tasks',
                label: '2/2',
                cards: [
                  {id: 'Card1', title: 'Write Blog', description: 'Can AI make memes', label: '30 mins'},
                  {id: 'Card2', title: 'Pay Rent', description: 'Transfer via NEFT', label: '5 mins'}
                ]
              },
              {
                id: 'lane2',
                title: 'Completed',
                label: '0/0',
                cards: []
              }
            ]
          }
          
        return (
          <div>
            <button className="btn btn-form" onClick={this.CreateKanban}>create new kanban</button>
            <Board className="kanban-board" data={data} laneDraggable="true" />
          </div>
        );
    }
    */
  }
}