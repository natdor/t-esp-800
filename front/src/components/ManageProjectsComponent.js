import React, {Component} from 'react';
import axios from "axios";
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import paginationFactory, { PaginationProvider,PaginationListStandalone,
    PaginationTotalStandalone,
    SizePerPageDropdownStandalone } from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
/*import {Link} from "react-router-dom";*/
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/Table.css';
const { SearchBar } = Search;


export default class ManageProjectsComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            description: "",
            repo_name: "",
            items:[],
            projects: [],
            val:'',
            columns:[{
                    dataField: 'name',
                    text: 'name'
                },{
                    dataField: 'created_at',
                    text: 'created at'
                },{
                    dataField: 'updated_at',
                    text: 'updated at'
                },
                {
                    dataField:'actions',
                    text: 'Actions',
                    formatter: this.actionFormatter
                }
            ]
        }
        this.selectRef = React.createRef();
        this.selectProject = this.selectProject.bind(this);
        this.handleBtnDelete = this.handleBtnDelete.bind(this);
        this.handleBtnEdit = this.handleBtnEdit.bind(this);
    }


    actionFormatter(cell, row) {

        return (
            <span>
                <a href={cell.view} style={{paddingRight:"5px"}}>
                    {/*<FontAwesomeIcon icon={faPen}/>*/}
                    view
                </a>
                <a href={ cell.edit} style={{paddingRight:"5px"}} onClick={this.handleBtnEdit}>
                    {/*<FontAwesomeIcon icon={faPen}/>*/}
                    Edit
                </a>

                <a href={ cell.delete } onClick={this.handleBtnDelete} >
                    {/*<FontAwesomeIcon icon={faTrash}/>*/}
                    Delete
                </a>
            </span>);

    }



   componentDidMount() {



       this.itemToSelect();

    }

  itemToSelect(){
        let user_id = localStorage.getItem('UserId');
       axios.get('http://localhost:8000/list/'+user_id+'/repos', {headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then((res) => {
           // console.log(res.data);
           axios.get('http://localhost:8000/list/'+user_id+'/'+res.data[0].name+'/projects', {headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then((res) => {

               console.log(res);
               this.setState({projects:res.data})
           });
            let i = 0;
            let items =[]
           res.data.map((item)=>{items.push({id: i, name: item.name}); i++} )

            this.setState({items:items})
        });
    }

    selectProject = (event) => {

        this.setState({val:event.target.value })
        let user_id = localStorage.getItem('UserId');
        axios.get('http://localhost:8000/list/'+user_id+'/'+event.target.value+'/projects', {headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then((res) => {
            console.log(res);
            this.setState({projects:res.data})
        });

    }


    handleBtnDelete = (event) => {
        event.preventDefault();
        let user_id = localStorage.getItem('UserId');
        let project_name = this.state[0].name;
        console.log();
        axios.delete('localhost:8000/delete/'+user_id+'/projects/'+project_name,{headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {

        console.log(response);
        }).catch(function(error){
        }).finally(function(){})

    }

    handleBtnEdit = (event) => {
        event.preventDefault();
        let project_id = localStorage.getItem('ProjectId');
        const {name, description} = this.state;
        console.log();
        axios.patch('localhost:8000/patch/projects/'+project_id,{
            name: name ,
            body: description
        
        },{headers: {'Authorization':  `${localStorage.getItem('UserToken')}`}}).then( ( response ) => {

        console.log(response);
        }).catch(function(error){
        }).finally(function(){})

    }






    render() {
        {
            const paginationOption = {
                sizePerPage: 5,
                custom: true,
                totalSize: this.state.projects.length
            };



            return (<div className="container-component" >
                <ToolkitProvider keyField="id" data={this.state.projects} columns={this.state.columns} search>
                    {
                        props =>

                            <PaginationProvider
                                pagination={paginationFactory(paginationOption)}
                            >
                                {
                                    ({
                                         paginationProps,
                                         paginationTableProps
                                     }) => (
                                        <div className="container">

                                            <div className="title-wrapper d-flex justify-content-between align-items-center"><h2>Manage projects</h2> {/* <span className="btn btn-form" data-toggle="modal" data-target="#addProject">Add</span> */}</div>
                                            <div className="search-container d-flex align-items-center justify-content-between">
                                                {/* <span className="btn btn-form" data-toggle="modal"  data-target="#createProject">Create Project</span> */}
                                                <select ref={this.selectRef} onChange={this.selectProject} name="project" id="project_selec">
                                                    {this.state.items.map((item) => <option  key={item.id} value={item.name}>{item.name}</option>)}
                                                </select>
                                                <span className="item-search">
                                                <SearchBar className="edit-user-tab-search" {...props.searchProps} />
                                            </span>
                                            </div>
                                            <BootstrapTable keyField="id" headerClasses={"text-center"}
                                                            rowStyle={{textAlign: "center"}} {...props.baseProps}
                                                            striped={true} hover={true} {...paginationTableProps} />
                                            <SizePerPageDropdownStandalone {...paginationProps} />
                                            <PaginationTotalStandalone {...paginationProps}/>
                                            <PaginationListStandalone {...paginationProps}/>
                                        </div>
                                    )
                                }
                            </PaginationProvider>

                    }
                </ToolkitProvider>
            </div>);

        }
    }
}