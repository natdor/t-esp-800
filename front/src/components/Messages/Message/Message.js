import React from 'react';

import './Message.css';

import ReactEmoji from 'react-emoji';
import queryString from "query-string";

const Message = ({ message: { text, user, id_team}, name, team_id }) => {
    let isSentByCurrentUser = false;
    let displayMessage = false;

    const trimmedName = name.trim().toLowerCase();
    console.log(name);

    if(user === trimmedName ) {

        isSentByCurrentUser = true;

    }

    if(id_team == team_id) {

        displayMessage = true;
    }

    return (
        displayMessage ? (
        isSentByCurrentUser
            ? (
                <div className="messageContainer justifyEnd">
                    <p className="sentText pr-10">{trimmedName}</p>
                    <div className="messageBox backgroundGreen">
                        <p className="messageText colorWhite">{ReactEmoji.emojify(text)}</p>
                    </div>
                </div>
            )
            : (
                <div className="messageContainer justifyStart">
                    <div className="messageBox backgroundLight">
                        <p className="messageText colorDark">{ReactEmoji.emojify(text)}</p>
                    </div>
                    <p className="sentText pl-10 ">{user}</p>
                </div>
            )
        ) : '');
}

export default Message;