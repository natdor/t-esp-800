import React from "react";

import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/Menus.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faProjectDiagram,faTable, faGlasses, faComments } from '@fortawesome/free-solid-svg-icons';


export default function Menu() {


    return (
        <div className="col-1 p-0">

            <div className="lateralMenu">
                <ul className="lateralMenuIn">
                    <li className="menu-item">
                        <a className="nav-link" href="/my-projects"><FontAwesomeIcon icon={faProjectDiagram}/><span className="linkText">My Projects</span></a>
                    </li>
                    <li className="menu-item">
                        <a className="nav-link" href="/kanban"><FontAwesomeIcon icon={faTable}/><span className="linkText">Kanban</span></a>
                    </li>
                    <li className="menu-item">
                        <a className="nav-link" href="/my-teams"><FontAwesomeIcon icon={faGlasses}/><span className="linkText">Review</span></a>
                    </li>
                    <li className="menu-item">
                        <a className="nav-link" href="/teams"><FontAwesomeIcon icon={faComments}/><span className="linkText">Chat</span></a>
                    </li>
                </ul>
            </div>

        </div>
    );
}
