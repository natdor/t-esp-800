import React,{Fragment} from 'react';
import ReactDom from 'react-dom';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import Login from './components/Login/LoginComponent';
import ManageProjects from './components/ManageProjectsComponent';
import MyProjects from './components/MyProjectsComponent';
import SignUp from './components/Registration/SignUpComponent';
import Kanban from './components/Kanban/Kanban';
import Teams from './components/Teams/Teams';
import SearchBar from './components/SearchBar';
import Menu from './components/Menu';
import Chat from "./components/Chat/Chat";

import './App.css';

const App =()=>(
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Redirect to='/login' />
                    </Route>
                    <Route exact path="/login">
                        <Login></Login>
                    </Route>
                    <Route exact path="/sign-up">
                        <SignUp></SignUp>
                    </Route>
                    <Fragment>
                        <SearchBar/>
                        <div className="container-fluid page p-0">
                            <div className="row page-in m-0">
                                <Menu/>
                                <Route exact path="/my-projects">
                                    <MyProjects></MyProjects>
                                </Route>
                                <Route path="/kanban/:id" component={Kanban}/>
                                <Route exact path="/kanban">
                                    <Kanban></Kanban>
                                </Route>
                                <Route exact path="/manage-projects">
                                    <ManageProjects></ManageProjects>
                                </Route>
                                <Route path="/chat"  component={Chat}/>
                                <Route path="/teams"  component={Teams}/>
                            </div>
                        </div>
                    </Fragment>
                </Switch>
            </Router>
        )

export default App;
