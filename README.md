# T-ESP-800

# Run the api server

npx nodemon rs server.js

#accéder au container
docker exec -it api-db

# Connect to mongodb server
mongo admin -u root -p root

# Create the api database

use api-db
db.init.insert({"name":"creation"})

# Create new db user

db.createUser(  
  {
    user: "devapi",
    pwd: "devapi",
    roles: [ { role: "readWrite", db: "api-db" } ]
  }
)

# Connect to mongo for the api database

mongo -u devapi -p devapi --authenticationDatabase "api-db"


# Routes


## Regular Login route
     
### POST | /user/login
	
	{
        
        email:String,
		password:String,
		
	}

    
 ## Github Login route
     

### GET | /user/auth/github

	
 ## Users routes
     

### GET | /users
### GET | /user/logout

Before signup you have to create a personal acces token: https://github.com/settings/tokens

### POST | /user/signup
	
	{
        login:String,
        email:String,
        firstname:String,
        lastname:String,
		password:String,
		token_git:String
	}
	
### GET | /user/:id_user/profile
### PUT | /user/edit
	{
        login:String,
        email:String,
        firstname:String,
        lastname:String,
		password:String,
		token_git:String
	}
	
### DELETE | /user/:id_user/delete
    
 ## Collaborators routes
     
### GET | /list/:id_user/collaborators/:repo_name
### POST | /add/collaborators
	{
		id_user: String,
		repo_name: String,
		username: String
	}
### DELETE | /delete/:id_user/collaborators/:repo_name/:username

    
## Repo routes
     

### GET | /list/:id_user/repos
	
### GET | /:id_user/repo/:repo_name
	
### POST | /add/repos
	{
		id_user:String,
		name: String,
		description: String,
		homepage: String,
		private: Bool,
		has_issues: Bool,
		has_projects: Bool,
		has_wiki: Bool
	}
	
### PATCH | /edit/repos
	
	{
		id_user:String,
		repo_name: String,
		name: String,
		description: String,
		homepage: String,
		private: Bool,
		has_issues: Bool,
		has_projects: Bool,
		has_wiki: Bool
	}
	
### DELETE | /delete/:id_user/repos/:repo_name
	
    
     ## Repo file
     


### GET | /:id_user/repo/files/:repo_name
### GET | /:id_user/repo/file/:repo_name/:path
### PUT | /repo/file
	{
		path: String,
		repo_name: String,
		message: String,
		content: String,
		sha: String,
		branch: String,
		committer: String,
		author: String
	}
    
    ## Projects routes
     

### GET | /list/:id_user/:repo_name/projects
### GET | /:id_user/project/:project_id
### POST | /add/projects
	{
		id_user:String,
		repo_name: String,
		name: req.body.name,
		body: req.body.description

	}
### PUT | /edit/projects
	{
		id_user:String,
		repo_name: String,
		name: req.body.name,
		body: req.body.description

	}
### DELETE | /delete/:id_user/projects/:project_id


    
## pullRequest routes
     

### GET | /list/:id_user/pulls/:repo
### POST | /add/pulls
	{
		id_user:String,
		repo_name: String,
		title: String,
		body: String,
		state: String,
		base: String
	}
	
### PUT | /edit/pulls
	{
		id_user:String,
		repo_name: String,
		pull_num:String,
		title: String,
		body: String,
		state: String,
		base: String
	}
	
### POST | /merge/pulls
	
	{
		id_user:String,
		repo_name: String,
		pull_num:String,
		commit_title: String,
		commit_message: String,
		sha: String,
		merge_method: String
	}

    
 ## Designs routes
     
	 
### GET |/list/designs
### GET | /:id_design/design
### POST | /add/designs
	
	{		
	name: String,
	design_url: String,
	user_id: String
	
	}
	
### PATCH | /edit/designs
	
	{		
	design_id: String,
	name: String,
	design_url: String,
	user_id: String
	}
### DELETE | /delete/:id_design/designs

    
## Comments routes
     

### GET |/list/comments/:design_id
### POST | /add/comments
	{
		content: String,
		design_id:String,
		id_user: String
	}
### PATCH | /edit/comments
	{
	content: String,
    comment_id: String
	}
### DELETE | /delete/:id_comment/comments


# Activity Diagram 

<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=5,IE=9" ><![endif]-->
<!DOCTYPE html>
<html>
<head>
<title>Activity Diagram</title>
<meta charset="utf-8"/>
</head>
<body><div class="mxgraph" style="max-width:100%;border:1px solid transparent;" data-mxgraph="{&quot;highlight&quot;:&quot;#0000ff&quot;,&quot;nav&quot;:true,&quot;resize&quot;:true,&quot;toolbar&quot;:&quot;zoom layers lightbox&quot;,&quot;edit&quot;:&quot;_blank&quot;,&quot;xml&quot;:&quot;&lt;mxfile host=\&quot;www.draw.io\&quot; modified=\&quot;2020-05-09T22:27:17.613Z\&quot; agent=\&quot;5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.5 Safari/605.1.15\&quot; etag=\&quot;HGoiRSjngRnvPB0SV7fx\&quot; version=\&quot;12.9.6\&quot; type=\&quot;google\&quot;&gt;&lt;diagram name=\&quot;activity diagram\&quot; id=\&quot;e7e014a7-5840-1c2e-5031-d8a46d1fe8dd\&quot;&gt;3VtJc6M4FP41PiYFCLwcs/dlqroqh5k+EiwbVTBihLykf/1IRrJBT3Zkm6VrckiFJ6Hl0/c2PTJCT6vdG4uL9C86x9ko8Oa7EXoeBcFkEojfUvBVCcJpVAmWjMwrkX8UvJPfWAk9JV2TOS4bHTmlGSdFU5jQPMcJb8hixui22W1Bs+asRbzEQPCexBmU/k3mPFVSfzw7NvzAZJmqqafBuGr4iJPPJaPrXM03CtBi/1M1r2I9ltpomcZzuq2J0MsIPTFKefXXaveEMwmthq167/VE62HdDOfc5QV1Tps4W6utv3NcCIkaqeRfGpNyS1ZZnIunx21KOH4v4kQ2bQUD4LTq/Q1mHO9qIrWMN0xXmLMv0UW3jsN7RRJFGj+YVs/b4xEEUwVcWkM/mihhrI59eRj9uHXxh9q9HYkIIAEQEC8IAkoAxLEVUljymPF3HnMpXJAse6IZZfveyNv/yM6c0U9ca1ksVAuELbgYNs9rgBZ6ADNkgQy1gNjYAbG5UCv1iLMPun05Ch73AtGQUkZ+05zHYqJHuXMiFPEhI8tcNH5QzulK9s7nD1KzhYwWOK8kynJMbSi/viqURb9XInfx7IunvXJiuQXvJPwlXbMEN3ghDnqJVa9JJZKbu+WIbNirQX5SIpZ0HME3NMM3hqhWp94yTvCwIKdDnYBDjedzOVOSCOA4OOEWKDz27Jvb1i2vTe1b4PAUbDejy723kb9SLPeWxXxB2aqHrR/cQh9bnzmoL9C5rrRsArVs6qhlDIsDIpum574FGA14DRmGl0RsXcxD8x54gLTV6YMHvg+2OyARppAIeuMDMAGGRy24uLPQQR93A5gauWucV516noV6B6Ez6id8m2kDZwanqw0B3wYGAn7EHOiEkxSIx1+1boXsUJ5eMDLWGzUibPFHNeC1DhgByqmIHFKv44g8DIeNyH0IBdS+7kNydCluY3u410NE7odd2KthQ3JkMWvI4iQiR7u2a56K1uKLY/KoqRqOxuYKg+DDzFRfe6g49Y3wH+uP0VMwehCrEGkZ2sUCVveQ/WKKB4ap7TNk92HemaQ4+ZRdFnJNdC1njTcyeLfB4W1ILNdQEA1Z5wD1Gtj7MIeDVqCbgM6mrJYEWh/hABEdTPiUMpHcyPtq1JG3ej2wpN+wf7j8z8aSsYUlg2WAAUyJ/l3jtWQFXhX8CwDFUrr6WJcw4pBXv0nnEYcfmeYGEskWpp27iXLGCuZIOYVMujjqiFW0keEF7ygI2RH+j2SoiHKrp197vp7jsmwTAFWvTSL9/Os4jHz8iRkRQGLmzP9DlaPG/+DCkOZOjdFyQuab1qalPOqQn+l5pu0mUpZM6guX3TOT02LA2NhGJJ1a9E0k44CRFzkR6Zqzdkl8BnVmGr4BnJlDZUsnzxnZ3yZUwOjK5/is+2KUV9ei6Hl2Gp4LfJlxv+IHk3vozSKLN/OjFsD6Hxa1bFbCFpRf6G7MgzIz4O8NhHmV5pvxSHsGQs9Ur2tVHYRH8Ggn0b0fDhjeIxjCljhmSTpSafIhHT5kOSICKuFtgTYNOeX2i0bB0oqwOu+8DTSvea8Salp9dx/dCmgwlj3nRnJas5bnldQDceQ8LtP9g+WbDZtPsRRG0GA+BTn4WytTTkJl5E1KfjufgrDJJzSzOBPIprAFXxK6FNYu9iXH/AMmHx1Vlix01Pa0QcfQkY59lZbMi/ipYSJcK0tmRoTMgVrLiAzb57WbEUGlVaUlfcC9lZYmwbClJQRzQ3UNKHSgcQuY0LlxGcjwhuCtkC0Y3Z9enMt3rBfOuCz1cFW4UTChfiIYtCSiENbwYlgNnk4svtNWe5q2YO2Qywd0A9XKG7c2wZW3NiG0eM43NH1ZvDsjog7ND8VcTd6db5Z4zApZSzbvzjfmUTxqrZzuUgyp0dIlprvek1o4pE3Rn8uhYHZv2AdXFs2+HanFDA9WduBBg68FZLp95bcCt5trM4OOPGivu/pUAA1X/lE4NcJJS0UfDVf+gZcFfWHjQ2xsH3H1VhoTj8d/Rqi08vgPH+jlPw==&lt;/diagram&gt;&lt;/mxfile&gt;&quot;}"></div>
<script type="text/javascript" src="https://www.draw.io/js/viewer.min.js"></script>
</body>
</html>

